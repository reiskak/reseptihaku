/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


window.Vue = require('vue');

//import BootstrapVue from 'bootstrap-vue'
//Vue.use(BootstrapVue)

import { LayoutPlugin } from 'bootstrap-vue'
Vue.use(LayoutPlugin)

import { FormRadioPlugin } from 'bootstrap-vue'
Vue.use(FormRadioPlugin)

import { CollapsePlugin } from 'bootstrap-vue'
Vue.use(CollapsePlugin)

import { ButtonPlugin } from 'bootstrap-vue'
Vue.use(ButtonPlugin)

import { CardPlugin } from 'bootstrap-vue'
Vue.use(CardPlugin)

import { ToastPlugin } from 'bootstrap-vue'
Vue.use(ToastPlugin)

import { FormGroupPlugin } from 'bootstrap-vue'
Vue.use(FormGroupPlugin)

import { FormCheckboxPlugin } from 'bootstrap-vue'
Vue.use(FormCheckboxPlugin)

import { FormInputPlugin } from 'bootstrap-vue'
Vue.use(FormInputPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Vuex from 'vuex';
Vue.use(Vuex);



const store = new Vuex.Store({
    state: {
        recipes: [],
        selectedIngredients: [],
        init: false,
        baseUrl: 'http://178.62.247.129/htyo/',
        baseIngredients: [],
        auth: false,
    },
    mutations: {
        updaterecipes(state,data) {
            state.init = true;
            return state.recipes = data;
        }
    }
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('ingredientlist', require('./components/IngredientList.vue').default);
Vue.component('IngrdientItem', require('./components/IngrdientItem.vue').default);
Vue.component('fridgeitems', require('./components/FridgeItems.vue').default);
Vue.component('recipelist', require('./components/RecipeList.vue').default);
Vue.component('recipeitem', require('./components/RecipeItem.vue').default);
Vue.component('favlist', require('./components/FavList.vue').default);
Vue.component('favbutton', require('./components/FavButton.vue').default);
Vue.component('ingredientselect', require('./components/IngredientSelect.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store: store,
});

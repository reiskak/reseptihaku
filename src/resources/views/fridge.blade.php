@extends('layouts.app')

@section('content')
    <b-container fluid>
        <b-row no-gutters>
            <b-col lg="12" >
                <div class="card">
                    <div class="card-header"><h2 class="card-title text-center">Fridge Search</h2></div>
                </div>
            </b-col>
        </b-row>
        <b-row no-gutters>
            <b-col md="3" class="ingcolumn"  ><fridgeitems></fridgeitems></b-col>
            <b-col lg="9" class="recipecolumn" ><recipelist :isauth={{Auth::check() ? 1 : 0}}></recipelist></b-col>
        </b-row>
    </b-container>
@endsection

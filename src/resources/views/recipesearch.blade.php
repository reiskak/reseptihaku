@extends('layouts.app')

@section('content')
    <b-container fluid>
        <b-row no-gutters>
            <b-col md="3" class="ingcolumn"  ><ingredientlist></ingredientlist></b-col>
            <b-col lg="9" class="recipecolumn" ><recipelist :isauth={{Auth::check() ? 1 : 0}}></recipelist></b-col>
        </b-row>
    </b-container>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('recipesearch');
});

Route::get('/recipesearch', function () {
    return view('recipesearch');
});

Auth::routes();

Route::get('/home', 'FavController@index')->name('home');
Route::get('/favorites', 'FavController@index');

Route::get('recipes/search', 'RecipeController@recipeSearch'); //
Route::get('ingredientsbyid/{q}', 'RecipeController@arrayIngredients');
Route::get('ingredients/{q}', 'RecipeController@ingredientByName');

Route::get('recipes/puppy', 'RecipeController@puppySearch');
Route::get('puppySearch/{ing}', 'RecipeController@puppySearch');

Route::post('fav/save','FavController@saveFav');
Route::post('fav/delete','FavController@deleteFav');
Route::get('fav','FavController@getFavs');

Route::get('fridge','FridgeController@index');
Route::post('fridge/save','FridgeController@addFridgeIngredient');
Route::post('fridge/delete','FridgeController@removeFridgeIngredient');
Route::get('fridge/recipes','FridgeController@getFridgeRecipes');
Route::get('fridge/all','FridgeController@listFridgeIngredients');

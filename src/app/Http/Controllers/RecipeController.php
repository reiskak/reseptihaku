<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;
use App\Ingredient;
use App\Cookingrecipe;
use App\Recipe;
use App\MongoRecipe;
use App\MongoIngredient;
use Illuminate\Support\Facades\Validator;
class RecipeController extends Controller
{
    // Hakee reseptejä tietokannasta annettujen IDitten perusteella
    // Get paramentrit: ing: pilkkuerotettu haettavien ainesosien id lista, ban: pilkkuerotettu kiellettyjen ainesosien id lista
    public function recipeSearch(Request $request) {
        // Validoidaan syötteet, pilkulla erotettuja lukuja molemmat, ban voi olla tyhjä.
        $validator = Validator::make($request->all(), [
            'ban' => 'nullable|regex:/^[0-9]+(,[0-9]+)*$/',
            'ing' => 'required|regex:/^[0-9]+(,[0-9]+)*$/',
        ]);
        if ($validator->fails()) {
            $response = array('response' => '', 'success'=>false);
            $response['response'] = $validator->messages();
            return $response;
        }else{
            // Otetaan annetut idt ja tehdään niistä numeerinen taulu.
            $idarray = explode(',', request('ing'));
            $intarr = array_map('intval',$idarray);
            // Tehdään myös mahdollisista kielletyistä ainesosita numeerinen taulu.
            $bannedIngredientsInt = [];
            if($request->has('ban'))
            {
                $bannedIngredients = explode(',', request('ban'));
                $bannedIngredientsInt = array_map('intval',$bannedIngredients);
            }
            $recipes = [];
            // Tehdään bitwise hakuoperaatio tietokantaan luoduilla tauluilla.
            if($request->has('name'))
            {
                $recipes = MongoRecipe::recipesByArray($intarr,$bannedIngredientsInt)->where('recipeName','like','%' . request('name') . '%')->paginate(10)->appends(request()->except('page'));
            }
            else
            {
                $recipes = MongoRecipe::recipesByArray($intarr,$bannedIngredientsInt)->paginate(10)->appends(request()->except('page'));
            }

            // Lisätään provideri frontendiä varten.
            $provider = collect(['provider' => 'local']);
            $return = $provider->merge($recipes);
            // Tallennetaan viimeisin haku sessioon.
            session(['lastSearchProvider' => 'local']);
            session(['lastSearch' => $return]);
            return $return;
        }

    }


    // Hakee ainesosien nimet ID:tten perusteella.
    // array:  pilkkuerotettu haettavien ainesosien id lista
    public function arrayIngredients($array)
    {
        $idarray = explode(',', $array);
        $intarr = array_map('intval',$idarray);
        return MongoIngredient::whereRaw(['_id' => ['$in' => $intarr]])->get();
    }
    // Haetaan ainesosia osittaisen nimen perusteella
    // $q: hakusana
    public function ingredientByName($q)
    {
        $data = MongoIngredient::where('ingredientName','like','%' . $q . '%')->get()->toArray();
        // Järjestetään nimen pituuden mukaan, MongoDBssä ei ollut tähän helppoa omaa ratkaisua.
        usort($data,function($a,$b) {
            return strlen($a['ingredientName'])-strlen($b['ingredientName']);
        });
        // palautetaan 10 lyhintä.
        return array_slice($data, 0, 10);
    }

    // Haku RecipePuppyn APIsta
    // Get parametrit: ing: pilkkuerotettu haettavien ainesosien lista, ban: pilkkuerotettu kiellettyjen ainesosien lista, page. sivunumero
    public function puppySearch(Request $request)
    {
        // Syötteitä ei tässä tapauksessa hirveästi auta validoida, koska ne voivat periaatteessa olla mitä vaan tekstiä.

            $client = new \GuzzleHttp\Client();
            $bannedIngredients = '';
            // Paginointiin liittyvää asiaa, RecipePuppyn API tarjoaa sivut, mutta ei mitään sivuihin liittyvää tuloksissa, niimpä teemme sen manuaalisesti
            $page = 1;
            if ($request->has('page')) {
                $page = intval(request('page'));
            }
            // Kielletyt ainekset RecipePuppyn formaattiin
            if ($request->has('ban') && \request('ban') != '') {
                $bannedIngredients = explode(',', request('ban'));
                foreach ($bannedIngredients as &$value) {
                    $value = '-' . $value;
                }
                $bannedIngredients = ',' . implode(',', $bannedIngredients);
            }
            // muotoillaan request URL
            $url = 'http://www.recipepuppy.com/api/?i=' . \request('ing') . $bannedIngredients . '&p=' . $page;
            $name = '';
            if($request->has('name'))
            {
                $name = request('name');
                $url .= '&q=' . $name;
            }
            // Haetaan vastaus puretaan se objektiksi
            $response = $client->get($url)->getBody();
            $json = json_decode($response);
            // Muunnetaan frontendin ymmärtämään mutoon
            $formatted = [];
            foreach ($json->results as $result) {
                $temp = [];
                $temp["recipeName"] = $result->title;
                $temp["link"] = $result->href;
                $temp["thumbnail"] = $result->thumbnail;
                $temp["ingredients"] = explode(',', $result->ingredients);
                array_push($formatted, $temp);
            }
            // Lisää manuaalista paginointia...
            $base = $request->url() . '?ing=' . request('ing');
            $firstpage = $base . '&name=' . $name . '&page=1';
            $nextpage = $base . '&name=' . $name . '&page=' . ($page + 1);
            $prevpage = null;
            if ($page > 1)
                $prevpage = $base . '&name=' . $name .  '&page=' . ($page - 1);
            if (request('ban')) {
                $firstpage .= '&ban=' . request('ban');
                $nextpage .= '&ban=' . request('ban');
                if ($prevpage != null)
                    $prevpage .= '&ban=' . request('ban');
            }
            // Yhdistetään kaikesta paginoinnista lopullinen objekti
            $return = array("data" => $formatted, "provider" => "puppy", "current_page" => $page, "first_page_url" => $firstpage, "per_page" => 10, "path" => $request->url(), "next_page_url" => $nextpage, "prev_page_url" => $prevpage, "last_page" => 100, "total" => 1000);
            // Tallennetaan viimeisin haku sessioon.
            session(['lastSearchProvider' => 'puppy']);
            session(['lastSearch' => $return]);
            return $return;
        }

}

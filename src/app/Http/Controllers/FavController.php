<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\MongoFavorite;
class FavController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('favorites');
    }
    // Palauttaa suosikkireseptit paginoituna.
    public function getFavs()
    {
        return MongoFavorite::where('userid', Auth::id())->paginate(10);
    }
    // Tallentaa suosikkireseptin tietokantaan
    // Post parametrit: id: tallennettavan reseptin indeksi viime haussa.

    public function deleteFav(Request $request)
    {
        // Varmistetaan että löytyy id
        $request->validate([
            'id' => 'required',
        ]);
        return MongoFavorite::where([
            ['userid', Auth::id()],
            ['_id', request('id')]
        ])->forceDelete();
    }

    public function saveFav(Request $request)
    {
        // Varmistetaan että löytyy id, ja että se on numeerinen
        $request->validate([
            'id' => 'required|numeric',
        ]);
        $recipeid = request('id');
        $data = session()->get('lastSearch');
        if(isset($data['data'][$recipeid]))
        {
            $fav = new MongoFavorite();
            $fav->provider = $data['provider'];
            $fav->userid = Auth::id();
            $fav->recipe = $data['data'][$recipeid];
            $fav->save();
        }
        return;
    }
}

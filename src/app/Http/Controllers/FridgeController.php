<?php

namespace App\Http\Controllers;
use App\MongoFridge;
use App\MongoIngredient;
use App\MongoRecipe;
use Auth;
use Illuminate\Http\Request;
class FridgeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('fridge');
    }

    // Hakee reseptit jääkaapistasi löytyvien ainesosien perusteella.
    public function getFridgeRecipes(){
        // Haetaan tallennetut ainesosat
        $all = MongoFridge::where('userid', Auth::id())->get()->toArray();;
        // Otetaan tuloksista pelkät IDt
        $ids = array_column($all, 'ingredientid');
        // Tehdään näistä numeerisia.
        $intarr = array_map('intval',$ids);
        //luodaan ainesosaarray, jota käytetään bitwise hakuoperaatiossa estämään kaikki muut ainesosat kuin mitä jääkaapista löytyy.
        $count = MongoIngredient::all()->count();
        $banarray = range(0, $count-1);
        foreach ($intarr as $value)
        {
            unset($banarray[$value]);
        }
        $banarray = array_merge($banarray);
        $recipes = MongoRecipe::BitsAllClear($banarray)->paginate(5);
        // Lisätään frontendiä varten local provider tieto.
        $provider = collect(['provider' => 'local']);
        $return = $provider->merge($recipes);
        // Päivitetään sessioon viimeisin haku, suosikkien tallentamista varten.
        session(['lastSearchProvider' => 'local']);
        session(['lastSearch' => $return]);
        return $return;
    }
    // Palauttaa kaikki jääkaappiin tallennetut ainesosat.
    public function listFridgeIngredients()
    {
        $all = MongoFridge::where('userid', Auth::id())->get()->toArray();;
        $ids = array_column($all, 'ingredientid');
        return redirect(action('RecipeController@arrayIngredients', implode(',', $ids))) ;
    }
    // Poistaa pyydetyn ainesosan jääkaapistasi.
    // Post parametrit: id: poistettavan ainesosan id
    public function removeFridgeIngredient(Request $request)
    {
        // Varmistetaan että löytyy id, ja että se on numeerinen
        $request->validate([
            'id' => 'required|numeric',
        ]);
         MongoFridge::where([
            ['userid', Auth::id()],
            ['ingredientid', request('id')]
        ])->forceDelete();
    }
    // Lisää pyydetyn ainesosan jääkaappiisi
    // Post parametrit: id: lisättävän ainesosan id, amt: ainesosan määrä, ei toteutettu mitään käyttöä.
    public function addFridgeIngredient(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'amt' => 'numeric',
        ]);
        $fridgeitem = new MongoFridge();
        $fridgeitem->userid = Auth::id();
        $fridgeitem->ingredientid = request('id');
        if($request->has('amt'))
        {
            $fridgeitem->amount = request('amt');
        }
        else
        {
            $fridgeitem->amount = 0;
        }
        $fridgeitem->save();
        return $fridgeitem;
    }
}

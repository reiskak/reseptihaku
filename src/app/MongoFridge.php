<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
// Jääkaappi modelli
class MongoFridge extends Model
{
  //  protected $hidden = ['_id'];
    protected $collection = 'fridge';
    protected $fillable = [
        'userid', 'ingredientid','amount'
    ];
}

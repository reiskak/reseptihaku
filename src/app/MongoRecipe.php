<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
// Resepti modelli
class MongoRecipe extends Model
{
    protected $hidden = ['binaryIngredients'];
    protected $collection = 'recipes';
    // Metodi Bitwise reseptihaulle tietokannasta, ottaa parametereinä haluttujen ainesosien sijainnit, ja mahdollisesti kiellettyjen ainesosien sijainnit
    public function scoperecipesByArray($query,$ingredints,$bannedIngredients = [])
    {
        if(count($bannedIngredients) > 0)
        {
            return $query->whereRaw(
                ['binaryIngredients' =>['$bitsAllSet' => $ingredints]]
            )->whereRaw(
                ['binaryIngredients' =>['$bitsAllClear' => $bannedIngredients]]
            );
        }
        else
        {
            return $query->whereRaw(
                ['binaryIngredients' =>['$bitsAllSet' => $ingredints]]
            );
        }
    }
    // Pelkillä kieletyillä osilla haku, jääkaappihakua vaten.
    public function scopeBitsAllClear($query,$ingredints)
    {
            return $query->whereRaw(
                ['binaryIngredients' =>['$bitsAllClear' => $ingredints]]
            );
    }
    // Funktio jolla haettiin resepteihin myös ainesosien nimet, totesin liian paljon hitaammaksi, haetaan nimet erikseen jos avataan resepti.
 /*
    public function scoperecipesByArraySlow($query,$array)
    {
        return $query->raw((function($collection) use($array)
        {
            return $collection->aggregate(
                [
                    ['$match' => ['binaryIngredients' =>['$bitsAllSet' => $array]]],
                    [
                        '$lookup' => [
                            'from'=>'ingredients',
                            'localField'=>'ingredients.ingredientId',
                            'foreignField'=>'_id',
                            'as'=>'ingredient',
                        ]
                    ],
                    [
                        '$project' =>
                            [
                                'recipeName' => '$recipeName',
                                'recipeId' => '$recipeId',
                                'ingredients' => [
                                    '$map' => [
                                        'input'=> '$ingredients',
                                        'in' => [
                                            'ingredientId'=> '$$this.ingredientId',
                                            'amount'=> '$$this.amount',
                                            'unit'=> '$$this.unit',
                                            'ingredientName' => [
                                                '$arrayElemAt' =>
                                                    ['$ingredient.ingredientName', [ '$indexOfArray' =>['$ingredient._id','$$this.ingredientId']]
                                                    ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                    ]
                ]
            );
        }));
    }*/
}

<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
// Suosikkiresepti modelli
class MongoFavorite extends Model
{
  //  protected $hidden = ['_id'];
    protected $collection = 'favorites';
    protected $fillable = [
        'provider', 'userid', 'recipe'
    ];
}

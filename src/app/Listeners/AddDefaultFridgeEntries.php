<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\MongoFridge;

class AddDefaultFridgeEntries
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    private $defaultings = [445,30,28,417,21,1710,772,97,94,881,6,251,795,2137,64,555,341,8,153,9,14];
    // Lisätään uusien tunnusten jääkaappiin tavaraa, testauksen helpottamiseksi.
    public function handle(Registered $event)
    {
        $user = $event->user;
        foreach ($this->defaultings as $ing)
        {
            $fridgeitem = new MongoFridge();
            $fridgeitem->userid = $user->getId();
            $fridgeitem->ingredientid = $ing;
            $fridgeitem->save();
        }
    }
}

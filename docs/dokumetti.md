# Raportti


### Sisällys:

* [Esittelyvideo](#esittelyvideo)
* [Johdanto](#johdanto)
* [Tietoa ohjelmasta](#tietoa-ohjelmasta)
* [Tietokanta](#tietokanta)
* [Tiedostorakenne](#tiedostorakenne)
* [Back end (Laravel)](#back)
* [Front end (Vue.js)](#front)
* [Bugit ja kehitysideat](#bugit)
* [Haasteet ja oppiminen](#haasteet)
* [Tekijä](#tekijat)
* [Pohdinta/Itsearvio](#itsearvio)

## Esittelyvideo

[VIDEO](https://youtu.be/gApFPt-CzTI)

## Johdanto

Olen jo pitempään miettinyt joskus tekeväni ohjelman, jolla voi etsiä reseptejä ainesosilla. Tämän kurssin harjoitustyön puitteissa ajattelin vihdoin kokeilevani, koska webohjelmointi ympäristö sopii oikein hyvin tälläisen ohjelman toteuttamiseen. 

Back end puoleksi valitsin Laravelin, koska sitä oli hyvin käyty läpi kurssilla ja olin jo oppinut käyttämään sitä hyvin. Front endiä varten päädyin opettelemaan Vue.js frameworkkiä, koska Reactia oli hyvin nopeasti kurssin lopussa ja Vue vaikutti pätevältä vaihtoehdolta mikä meni hyvin Laravelin kanssa yhteen.


## Tietoa sivustosta

Sivustolla voi tosiaan hakea reseptejä ainesosilla. Vaihtoehtoina on etsiä paikallisesta tietokannasta tai eri API palvelujen kautta. Sivusto listaa löydetyt reseptit ja niiden ainesosat ja antaa mahdollisuuden mennä katsomaan reseptiä tarkemmin alkuperäisestä lähteestä. Koko reseptejä sivustolta ei saa, koska nämä voivat olla tekijänoikeussuojattuja, toisin kuin pelkkä ainesosalista.

Lisäksi rekisteröitymisen jälkeen sivustolla voi tallentaa tiettyjä reseptejä suosikkilistalle, tai tehdä omassa profiilissa jääkaapin sisällöstään listan ja hakea satunnaista reseptiä mitä näistä voi tehdä.


## Tietokanta

Tietokannaksi päätyi monen mutkan jälkeen MongoDB NOSQL tietokanta. Tämä johtuen kaverini kanssa miettimästäni tehokkaasta tavasta säilöä ja etsiä reseptejä bitwise operaatioilla, minkä toteutuksessa oli erilaisia ongelmia perinteisimillä toteutuksilla. Tämä ei kuitenkaan varsinaisesti liity kurssin sisältöön, joten jossain vaiheessa päädyin vaan tähän nykyiseen ratkaisuun. NOSQL ei ole varsinainen relaatiotietokanta, joten relaatiokaaviota ei ole, mutta seuraavaksi kuvio tietokannan perusrakenteesta.

```plantuml
database recipedb as "
MongoDB
----
recipedb
"

entity recipes {
  * _id
  --
  * recipeName
  * recipeId
  * binaryIngredients
  * ingredients
  > ingredientId
  > amount
  > unit
}

entity ingredients{
  * _id
  --
  * ingredientName
}

entity favorites {
  * _id
  --
  * provider
  * userid
  * recipe
}


entity fridge{
  * _id
  --
  * ingredientid
  * userid
  * amount
}

entity User{
  * _id
  --
  * name
  * email
  * password
  * role
}

recipedb -- recipes
recipedb -- ingredients
recipedb -- favorites
recipedb -- fridge
recipedb -- User
```

## Tiedostorakenne

En ole ennen tälläisestä yrittänyt kuviota tehdä, mutta seuraavassa yritän hahmottaa sivuston rakennetta, eli kolme pääsivua plus laravelin auth, mitkä koostuvat erilaisista vue komponenteista.

```plantuml
object app.blade.php


object recipesearch.blade.php
object RecipeList.vue
object IngredientList.vue
object autocomplete
object RecipeItem.vue
object FavButton.vue
object IngredientItem.vue
object IngredientSelect.vue

recipesearch.blade.php -- RecipeList.vue
recipesearch.blade.php -- IngredientList.vue
RecipeList.vue -- RecipeItem.vue
RecipeItem.vue -- FavButton.vue
IngredientList.vue -- autocomplete
IngredientList.vue -- IngredientItem.vue
IngredientList.vue -- IngredientSelect.vue


object favorites.blade.php
object FavList.vue

favorites.blade.php -- FavList.vue
FavList.vue -- RecipeItem.vue

object fridge.blade.php
object FridgeItems.vue

fridge.blade.php -- FridgeItems.vue
FridgeItems.vue -- autocomplete
FridgeItems.vue -- IngredientItem.vue
fridge.blade.php -- RecipeList.vue

object auth

app.blade.php -- recipesearch.blade.php
app.blade.php -- favorites.blade.php
app.blade.php -- fridge.blade.php
app.blade.php -- auth
```

<a name="back"/>

## Back end(Laravel)

Serveripuoli tässä projektissa on tosiaan toteutettu Laravel-frameworkillä. Serveripuolella on toteutettu tietokantapuolta monille eri toiminoille ja näistä tarjotaan rajapintaa, mistä Front end voi hakea dataa. Erityisesti tässä puolessa työtä tuotti erilaisten hakujen tekeminen tehokkaasti. Aikaa kului myös MongoDB tietokanta integraation hienouksien opetteluun([laravel-mongodb](https://github.com/jenssegers/laravel-mongodb)).
Seuraavassa listaa toteutetuista rajapinnoista.
 * Ainesosien haku nimellä paikallisesta tietokannasta. (ingredients/{q} route, q: hakusana)
 * Reseptin hakeminen ainesosien IDillä paikallisesta tietokannasta, mahdollisuus myös mustalistata ainesosia. (recipes/search route, get parametrit ing:pilkkuerotettu id lista, ban: pilkkuerotettu id lista)
 * Reseptin hakeminen ainesosilla [RecipePuppyn](http://www.recipepuppy.com/about/api/) APIsta ja tulosten muuntaminen frontendin ymmärtämään muotoon.
 (recipes/puppy route, get parametrit ing:pilkkuerotettu ainesosa lista, ban: pilkkuerotettu ainesosa lista, page: sivunumero)
 * Kirjautuneena reseptin tallentaminen ja poistaminen omista suosikeista ja suosikkien listaus. (fav/save, /fav/delete, fav/)
 * Kirjautuneena oman jääkaapin sisällön tallentaminen ja muokkaus.(/fridge/, /fridge/save, /fridge/delete)
 * Kirjautuneena reseptihaku oman jääkaapin sisällön mukaan, missä haetaan vain reseptejä mitä jääkaapin aineista voi tehdä. (/fridge/recipes)

<a name="front"/>

## Front end(Vue.js)

Javascript puoli tässä toteutuksessa on tehty Vue.JS frameworkillä, joka oli itselle täysin uusi tuttavuus. Tutustuin siihen noin kurssin puolivälissä ja halusin opetella sitä lisää. Tämän vuoksi komponeteissa voi olla vähän kirjavaakin koodia, koska opin tehdessäni ja en kerinnyt korjaamaan kaikkea alussa koodaamaani koodia paremmanksi. Seuraavaksi vähän sivuston eri osioista.

### reseptihaku

Haku sivulla aineosien lisääminen haku on toteutettu [autocompelete-vuen](https://github.com/trevoreyre/autocomplete/tree/master/packages/autocomplete-vue) pohajalta ja käyttää back endin ainesosa rajapintaa. Tällä voidaan lisätä ainesosia hakuun, joka dynaamisesti päivittää reseptituloksia käyttäen back endin reseptihaku rajapintaa. Kirjautuneena näytetään myös nappula mikä on antaa mahdollisuuden tallentaa reseptin suosikkeihin.

Toteutettu myös mahdollisuus hakea ulkopuolisen [RecpipePuppy APIn](http://www.recipepuppy.com/about/api/) kautta. Käyttää back end puolen tätä varten toteutettua rajapintaa, joka muuttaa RecpipePuppyn vaustaukset front endin paremmin ymmärtämään muotoon.

### suosikit

Simppeli sivu millä voi katsella suosikkeja ja tarvittaessa poistaa niitä, käyttää pääosin hyväksi haussa käytettyjä vue komponentteja.

### jääkaappihaku

Saman tyylinen kuin reseptihaku, mutta vaatii kirjautumisen ja haku on erityyppinen. Tällä sivulla lisäät ainesosia jääkaappiisi, ja sivu hakee dynaamisesti reseptejä, mitä voisit tehdä jääkaappisi sisällöstä. Sivulle rekistöröityessä palvelinpuoli lisää automaattisesti jääkaappiisi jotain perusaineita, jotta ohjelmaa olisi helpompi testata.

### Bugit

* Jos reseptissä on vähän ainesosia, "Check recipe out" nappula ja Allrecipes.com teksti menee päällekkäin

### Kehitysideat

Ideoita oli paljonkin, ehkä toteutan lisää, kun on enemmän aikaa. Seuraavaksi vähän listaa

* Ainesosien jakaminen geneerisempiin kategorioihin haussa. Esim. Eggs sisältäisi kaikki alaversiot kuten Egg yolk. Aikaavievää manuaalista työtä.
* Haun rajoittaminen ruokalajin mukaan. Tähän oli jotain algoritmejä millä jakaa ruokia ainesosien mukaan eri kategorioihin, mutta en ehtinyt tutkia kunnolla.
* Jääkaappihaun parantaminen. Esim. vähintään x määrä ainesosia jääkaapista, olisi vaatinut tietokannan vaihtamista, joten jätin aikataulun takia tekemättä.
* Eri API palveluiden lisääminen, mutta näissä oli suurimassa osassa niin tiukat rajoitukset ilmaiskäyttäjille, että jätin väliin.
* Ulkonäön kehittäminen, koodin siistiminen ja refactorointi...


<a name="haasteet"/>

## Projektin haasteet ja mitä opittu

Projektin suurimmat haasteet liittyivät koronatilanteen aiheuttamiin aikatauluhaasteisiin. Motivaatiota oli opetella eri tekniikoita, mutta aikataulutus oli hankalaa. Perusominaisuudet sain suhteellisen aikaisessa vaiheessa tehtyä, mutta olisin halunnut jatkokehittää ja hioa ulkoasua enemmän. Opin kuitenkin paljon PHPstä, Javascriptistä ja etenkin Laravel ja Vue ympäristöistä.

<a name="tekijat"/>

## Tekijä ja aikataulu

Projektin on toteuttanut yksin Reima Ojala. Aikataulullisesti aikaa kului alkuvaiheessa paljon eri tietokantojen testaamiseen ja projektin kannalta olennaisempien Laravel rajapintojen toteuttamiseen. Tämän jälkeen aloin opiskelemaan ja toteuttamaan Vue.JS puolta, jonka opettelu vei aluksi aikaa, mutta osoittautui oikein hyväksi ja nopeaksi kehitysympäristöksi.

En varsinaisesti tunteja laskenut, koska lähdin tekemään tätä ensin enemmän kiinnostuspohjalta. Tehtävistä oli jo sen verran hyvät pisteet alla, että en varsinaisesti ottanut stressiä itse harjoitustyöstä. Arvioisin kyllä silti, että kokonaistunnit uusien asioiden opiskelu mukaan luettuna pyörii jossain 100 tunnin paikkeilla.

<a name="itsearvio"/>

## Pohdinta/Itsearvio

Työ oli oikein mukava toteuttaa ja olisin mielelläni tehnyt enemmänkin, mutta muiden kurssien työmäärien ja koronan aiheuttamien aikataulu ongelmien takia jätin tämän vähän vähemmälle. Olen super itsekriittinen ihminen ja tämän takia huono itsearvioimaan, mutta sanoisin työn tässäkin kunnossa olevan 3-4 arvoinen. Työssä on kuitenkin paljon ns. näkymätöntä työtä, kuten uuden frameworkin opettelu web ohjelmoinnin puolella. 


# Reseptihaku

Web-palvelinohjelmointi(TTMS0900) ja Web-ohjelmointi(TTMS0500) kurssien harjoitustyö

Tekijä: Reima Ojala(M3074) m3074@student.jamk.fi

Live versio: [http://178.62.247.129/htyo/](http://178.62.247.129/htyo/)

Koodi: Löytyy tästä reposta.

Video: [VIDEO](https://youtu.be/gApFPt-CzTI)

Dokumentaatio: [https://gitlab.labranet.jamk.fi/M3074/reseptihaku/-/blob/master/docs/dokumetti.md](https://gitlab.labranet.jamk.fi/M3074/reseptihaku/-/blob/master/docs/dokumetti.md)
